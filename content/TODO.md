# TODO

## Tasks

### Urgent

1. add links from "Waiting List" to categories
2. add more links to "Browser Exploitation"
3. add more links to "Mobile Exploitation"
4. rename "Mitigations" sections
5. rename all section names, change categories
6. Browser Exploitation: add columns \(software version, vulnerability type\)
7. update and sort out "Various Sutff" section
8. split categories by pages

### Later

1. Secure Coding: add more links
2. Heap-Fuzzing: add more links
3. Hardware: add categories
4. Heap: sort out
5. update missing CVEs
6. fix dead links, move to webarchive
7. update "Malware" section
8. add more ancient links
9. rewrite to use nunjucks template

## Waiting List

These links are about to be added. ~380 Links to go...

* Articles from [https://scarybeastsecurity.blogspot.de/](https://scarybeastsecurity.blogspot.de/)
* Articles from [https://modexp.wordpress.com/](https://modexp.wordpress.com/)
* Articles from [https://lazytyped.blogspot.de/?m=1](https://lazytyped.blogspot.de/?m=1)
* Articles from [http://tukan.farm/2016/07/26/ptmalloc-fanzine/](http://tukan.farm/2016/07/26/ptmalloc-fanzine/)
* [https://bugs.chromium.org/p/chromium/issues/list?can=1&q=Pwn2Own&colspec=ID+Pri+M+Stars+ReleaseBlock+Component+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=ids](https://bugs.chromium.org/p/chromium/issues/list?can=1&q=Pwn2Own&colspec=ID+Pri+M+Stars+ReleaseBlock+Component+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=ids)
* [http://robert.ocallahan.org](http://robert.ocallahan.org)

### 2015

[https://cloudblogs.microsoft.com/microsoftsecure/2015/06/17/understanding-type-confusion-vulnerabilities-cve-2015-0336/](https://cloudblogs.microsoft.com/microsoftsecure/2015/06/17/understanding-type-confusion-vulnerabilities-cve-2015-0336/)

### 2016 \(19\)

[http://blog.vectranetworks.com/blog/microsoft-windows-printer-wateringhole-attack](http://blog.vectranetworks.com/blog/microsoft-windows-printer-wateringhole-attack)

[https://blog.fortinet.com/2016/07/20/analysis-of-cve-2016-4203-adobe-acrobat-and-reader-cooltype-handling-heap-overflow-vulnerability](https://blog.fortinet.com/2016/07/20/analysis-of-cve-2016-4203-adobe-acrobat-and-reader-cooltype-handling-heap-overflow-vulnerability)

[https://census-labs.com/news/2016/07/22/android-stagefright-impeg2d\_dec\_pic\_data\_thread-overflow/](https://census-labs.com/news/2016/07/22/android-stagefright-impeg2d_dec_pic_data_thread-overflow/)

[https://www.evonide.com/how-we-broke-php-hacked-pornhub-and-earned-20000-dollar/](https://www.evonide.com/how-we-broke-php-hacked-pornhub-and-earned-20000-dollar/)

[http://keenlab.tencent.com/en/2016/07/29/The-Journey-of-a-complete-OSX-privilege-escalation-with-a-single-vulnerability-Part-1/](http://keenlab.tencent.com/en/2016/07/29/The-Journey-of-a-complete-OSX-privilege-escalation-with-a-single-vulnerability-Part-1/)

[http://blog.quarkslab.com/xen-exploitation-part-3-xsa-182-qubes-escape.html](http://blog.quarkslab.com/xen-exploitation-part-3-xsa-182-qubes-escape.html)

[https://blog.xyz.is/2016/webkit-360.html](https://blog.xyz.is/2016/webkit-360.html)

[https://blog.fortinet.com/2016/08/17/deep-analysis-of-cve-2016-3820-remote-code-execution-vulnerability-in-android-mediaserver](https://blog.fortinet.com/2016/08/17/deep-analysis-of-cve-2016-3820-remote-code-execution-vulnerability-in-android-mediaserver)

[https://blog.xyz.is/2016/vita-netps-ioctl.html](https://blog.xyz.is/2016/vita-netps-ioctl.html)

[https://sektioneins.de/en/blog/16-09-02-pegasus-ios-kernel-vulnerability-explained.html](https://sektioneins.de/en/blog/16-09-02-pegasus-ios-kernel-vulnerability-explained.html)

[https://googleprojectzero.blogspot.de/2016/09/return-to-libstagefright-exploiting.html](https://googleprojectzero.blogspot.de/2016/09/return-to-libstagefright-exploiting.html)

[https://labs.nettitude.com/blog/analysing-the-null-securitydescriptor-kernel-exploitation-mitigation-in-the-latest-windows-10-v1607-build-14393/](https://labs.nettitude.com/blog/analysing-the-null-securitydescriptor-kernel-exploitation-mitigation-in-the-latest-windows-10-v1607-build-14393/)

[https://info.lookout.com/rs/051-ESQ-475/images/pegasus-exploits-technical-details.pdf](https://info.lookout.com/rs/051-ESQ-475/images/pegasus-exploits-technical-details.pdf)

[http://keenlab.tencent.com/en/2016/11/18/A-Link-to-System-Privilege/](http://keenlab.tencent.com/en/2016/11/18/A-Link-to-System-Privilege/)

[https://scarybeastsecurity.blogspot.de/2016/11/0day-poc-risky-design-decisions-in.html](https://scarybeastsecurity.blogspot.de/2016/11/0day-poc-risky-design-decisions-in.html)

[https://googleprojectzero.blogspot.de/2016/12/bitunmap-attacking-android-ashmem.html](https://googleprojectzero.blogspot.de/2016/12/bitunmap-attacking-android-ashmem.html)

[http://blog.trendmicro.com/trendlabs-security-intelligence/one-bit-rule-system-analyzing-cve-2016-7255-exploit-wild/](http://blog.trendmicro.com/trendlabs-security-intelligence/one-bit-rule-system-analyzing-cve-2016-7255-exploit-wild/)

[http://srcincite.io/blog/2016/12/13/word-up-microsoft-word-onetabledocumentstream-underflow.html](http://srcincite.io/blog/2016/12/13/word-up-microsoft-word-onetabledocumentstream-underflow.html)

[https://googleprojectzero.blogspot.de/2016/12/chrome-os-exploit-one-byte-overflow-and.html](https://googleprojectzero.blogspot.de/2016/12/chrome-os-exploit-one-byte-overflow-and.html)

### 2017 \(126\)

[https://blogs.technet.microsoft.com/mmpc/2017/01/13/hardening-windows-10-with-zero-day-exploit-mitigations/](https://blogs.technet.microsoft.com/mmpc/2017/01/13/hardening-windows-10-with-zero-day-exploit-mitigations/)

[https://googleprojectzero.blogspot.de/2017/02/lifting-hyper-visor-bypassing-samsungs.html](https://googleprojectzero.blogspot.de/2017/02/lifting-hyper-visor-bypassing-samsungs.html)

[http://blog.quarkslab.com/analysis-of-ms16-104-url-files-security-feature-bypass-cve-2016-3353.html](http://blog.quarkslab.com/analysis-of-ms16-104-url-files-security-feature-bypass-cve-2016-3353.html)

[https://www.endgame.com/blog/chakra-exploit-and-limitations-modern-mitigation-techniques](https://www.endgame.com/blog/chakra-exploit-and-limitations-modern-mitigation-techniques)

[https://medium.com/@justin.schuh/securing-browsers-through-isolation-versus-mitigation-15f0baced2c2\#.6948zz5lj](https://medium.com/@justin.schuh/securing-browsers-through-isolation-versus-mitigation-15f0baced2c2#.6948zz5lj)

[https://samdb.xyz/revisiting-windows-security-hardening-through-kernel-address-protection/](https://samdb.xyz/revisiting-windows-security-hardening-through-kernel-address-protection/)

[https://medium.com/@mxatone/mitigation-bounty-4-techniques-to-bypass-mitigations-2d0970147f83\#.y0v90tw9k](https://medium.com/@mxatone/mitigation-bounty-4-techniques-to-bypass-mitigations-2d0970147f83#.y0v90tw9k)

[https://ricklarabee.blogspot.de/2017/01/virtual-memory-page-tables-and-one-bit.html](https://ricklarabee.blogspot.de/2017/01/virtual-memory-page-tables-and-one-bit.html)

[https://blogs.windows.com/msedgedev/2017/03/23/strengthening-microsoft-edge-sandbox/\#iyhRpeiGze7ZohQt.97](https://blogs.windows.com/msedgedev/2017/03/23/strengthening-microsoft-edge-sandbox/#iyhRpeiGze7ZohQt.97)

[https://a13xp0p0v.github.io/2017/03/24/CVE-2017-2636.html](https://a13xp0p0v.github.io/2017/03/24/CVE-2017-2636.html)

[https://googleprojectzero.blogspot.de/2017/04/over-air-exploiting-broadcoms-wi-fi\_4.html](https://googleprojectzero.blogspot.de/2017/04/over-air-exploiting-broadcoms-wi-fi_4.html)

[https://blogs.technet.microsoft.com/mmpc/2017/03/27/detecting-and-mitigating-elevation-of-privilege-exploit-for-cve-2017-0005/](https://blogs.technet.microsoft.com/mmpc/2017/03/27/detecting-and-mitigating-elevation-of-privilege-exploit-for-cve-2017-0005/)

[https://info.lookout.com/rs/051-ESQ-475/images/lookout-pegasus-android-technical-analysis.pdf](https://info.lookout.com/rs/051-ESQ-475/images/lookout-pegasus-android-technical-analysis.pdf)

[https://scarybeastsecurity.blogspot.de/2017/05/proving-missing-aslr-on-dropboxcom-and.html](https://scarybeastsecurity.blogspot.de/2017/05/proving-missing-aslr-on-dropboxcom-and.html)

[https://blogs.technet.microsoft.com/askpfeplat/2017/04/24/windows-10-memory-protection-features/](https://blogs.technet.microsoft.com/askpfeplat/2017/04/24/windows-10-memory-protection-features/)

[https://bugs.chromium.org/p/project-zero/issues/detail?id=1252&desc=5](https://bugs.chromium.org/p/project-zero/issues/detail?id=1252&desc=5)

[https://www.embedi.com/files/white-papers/Silent-Bob-is-Silent.pdf](https://www.embedi.com/files/white-papers/Silent-Bob-is-Silent.pdf)

[https://www.zerodayinitiative.com/blog/2017/5/4/auditing-adobe-reader-the-open-source-attack-surface-in-closed-source-software](https://www.zerodayinitiative.com/blog/2017/5/4/auditing-adobe-reader-the-open-source-attack-surface-in-closed-source-software)

[https://bugzilla.mozilla.org/show\_bug.cgi?id=1299686](https://bugzilla.mozilla.org/show_bug.cgi?id=1299686)

[https://bugzilla.mozilla.org/show\_bug.cgi?id=1287266](https://bugzilla.mozilla.org/show_bug.cgi?id=1287266)

[https://snf.github.io/2017/05/04/exploit-protection-i-page-heap/](https://snf.github.io/2017/05/04/exploit-protection-i-page-heap/)

[https://googleprojectzero.blogspot.de/2017/04/exception-oriented-exploitation-on-ios.html](https://googleprojectzero.blogspot.de/2017/04/exception-oriented-exploitation-on-ios.html)

[https://googleprojectzero.blogspot.de/2017/04/exploiting-net-managed-dcom.html](https://googleprojectzero.blogspot.de/2017/04/exploiting-net-managed-dcom.html)

[https://grsecurity.net/the\_infoleak\_that\_mostly\_wasnt.php](https://grsecurity.net/the_infoleak_that_mostly_wasnt.php)

[https://www.endgame.com/blog/disarming-control-flow-guard-using-advanced-code-reuse-attacks](https://www.endgame.com/blog/disarming-control-flow-guard-using-advanced-code-reuse-attacks)

[https://struct.github.io/oilpan\_metadata.html](https://struct.github.io/oilpan_metadata.html)

[https://phoenhex.re/2017-06-09/pwn2own-diskarbitrationd-privesc](https://phoenhex.re/2017-06-09/pwn2own-diskarbitrationd-privesc)

[https://blog.fortinet.com/2017/06/04/an-inside-look-at-cve-2017-0199-hta-and-scriptlet-file-handler-vulnerability](https://blog.fortinet.com/2017/06/04/an-inside-look-at-cve-2017-0199-hta-and-scriptlet-file-handler-vulnerability)

[https://risksense.com/\_api/filesystem/468/EternalBlue\_RiskSense-Exploit-Analysis-and-Port-to-Microsoft-Windows-10\_v1\_2.pdf](https://risksense.com/_api/filesystem/468/EternalBlue_RiskSense-Exploit-Analysis-and-Port-to-Microsoft-Windows-10_v1_2.pdf)

[https://bugs.chromium.org/p/project-zero/issues/detail?id=1258](https://bugs.chromium.org/p/project-zero/issues/detail?id=1258)

[https://blogs.technet.microsoft.com/srd/2017/06/29/eternal-champion-exploit-analysis/](https://blogs.technet.microsoft.com/srd/2017/06/29/eternal-champion-exploit-analysis/)

[https://blogs.technet.microsoft.com/srd/2017/06/20/tales-from-the-msrc-from-pixels-to-poc/](https://blogs.technet.microsoft.com/srd/2017/06/20/tales-from-the-msrc-from-pixels-to-poc/)

[https://blogs.technet.microsoft.com/mmpc/2017/06/16/analysis-of-the-shadow-brokers-release-and-mitigation-with-windows-10-virtualization-based-security/](https://blogs.technet.microsoft.com/mmpc/2017/06/16/analysis-of-the-shadow-brokers-release-and-mitigation-with-windows-10-virtualization-based-security/)

[https://www.thezdi.com/blog/2017/6/26/use-after-silence-exploiting-a-quietly-patched-uaf-in-vmware](https://www.thezdi.com/blog/2017/6/26/use-after-silence-exploiting-a-quietly-patched-uaf-in-vmware)

[http://acez.re/the-weak-bug-exploiting-a-heap-overflow-in-vmware/](http://acez.re/the-weak-bug-exploiting-a-heap-overflow-in-vmware/)

[https://www.coresecurity.com/blog/solving-post-exploitation-issue-cve-2017-7308](https://www.coresecurity.com/blog/solving-post-exploitation-issue-cve-2017-7308)

[https://blogs.technet.microsoft.com/srd/2017/07/20/englishmansdentist-exploit-analysis/](https://blogs.technet.microsoft.com/srd/2017/07/20/englishmansdentist-exploit-analysis/)

[https://cyber.wtf/2017/07/28/negative-result-reading-kernel-memory-from-user-mode/amp/](https://cyber.wtf/2017/07/28/negative-result-reading-kernel-memory-from-user-mode/amp/)

[https://github.com/MortenSchenk/BHUSA2017/blob/master/us-17-Schenk-Taking-Windows-10-Kernel-Exploitation-To-The-Next-Level–Leveraging-Write-What-Where-Vulnerabilities-In-Creators-Update-wp.pdf](https://github.com/MortenSchenk/BHUSA2017/blob/master/us-17-Schenk-Taking-Windows-10-Kernel-Exploitation-To-The-Next-Level–Leveraging-Write-What-Where-Vulnerabilities-In-Creators-Update-wp.pdf)

[https://securingtomorrow.mcafee.com/mcafee-labs/analyzing-cve-2017-0190-wmf-flaws-can-lead-data-theft-code-execution/\#sf101390209](https://securingtomorrow.mcafee.com/mcafee-labs/analyzing-cve-2017-0190-wmf-flaws-can-lead-data-theft-code-execution/#sf101390209)

[https://tyranidslair.blogspot.de/2017/07/dg-on-windows-10-s-executing-arbitrary.html](https://tyranidslair.blogspot.de/2017/07/dg-on-windows-10-s-executing-arbitrary.html)

[https://blog.trailofbits.com/2017/08/02/microsoft-didnt-sandbox-windows-defender-so-i-did/](https://blog.trailofbits.com/2017/08/02/microsoft-didnt-sandbox-windows-defender-so-i-did/)

[https://googleprojectzero.blogspot.de/2017/08/windows-exploitation-tricks-arbitrary.html](https://googleprojectzero.blogspot.de/2017/08/windows-exploitation-tricks-arbitrary.html)

[https://beingwinsysadmin.blogspot.de/2017/07/bug-windows-10-default-user-profile-is.html](https://beingwinsysadmin.blogspot.de/2017/07/bug-windows-10-default-user-profile-is.html)

[https://comsecuris.com/blog/posts/path\_of\_least\_resistance/](https://comsecuris.com/blog/posts/path_of_least_resistance/)

[https://www.zerodayinitiative.com/blog/2017/8/1/pythonizing-the-vmware-backdoor](https://www.zerodayinitiative.com/blog/2017/8/1/pythonizing-the-vmware-backdoor)

[https://sensepost.com/blog/2017/abusing-gdi-objects-for-ring0-primitives-revolution/](https://sensepost.com/blog/2017/abusing-gdi-objects-for-ring0-primitives-revolution/)

[https://www.zerodayinitiative.com/blog/2017/8/9/the-blue-frost-security-challenge-an-exploitation-journey-for-fun-and-free-drinks](https://www.zerodayinitiative.com/blog/2017/8/9/the-blue-frost-security-challenge-an-exploitation-journey-for-fun-and-free-drinks)

[http://blog.talosintelligence.com/2017/08/windbg-and-javascript-analysis.html](http://blog.talosintelligence.com/2017/08/windbg-and-javascript-analysis.html)

[https://blogs.technet.microsoft.com/srd/2017/08/09/moving-beyond-emet-ii-windows-defender-exploit-guard/](https://blogs.technet.microsoft.com/srd/2017/08/09/moving-beyond-emet-ii-windows-defender-exploit-guard/)

[https://tyranidslair.blogspot.de/2017/08/the-art-of-becoming-trustedinstaller.html](https://tyranidslair.blogspot.de/2017/08/the-art-of-becoming-trustedinstaller.html)

[https://googleprojectzero.blogspot.de/2017/08/bypassing-virtualbox-process-hardening.html](https://googleprojectzero.blogspot.de/2017/08/bypassing-virtualbox-process-hardening.html)

[https://alephsecurity.com/2017/08/30/untethered-initroot/](https://alephsecurity.com/2017/08/30/untethered-initroot/)

[https://kitctf.de/writeups/hitb2017/babyqemu](https://kitctf.de/writeups/hitb2017/babyqemu)

[https://github.com/hatRiot/token-priv/blob/master/abusing\_token\_eop\_1.0.txt](https://github.com/hatRiot/token-priv/blob/master/abusing_token_eop_1.0.txt)

[https://blog.zimperium.com/ziva-video-audio-ios-kernel-exploit/](https://blog.zimperium.com/ziva-video-audio-ios-kernel-exploit/)

[https://blog.checkpoint.com/wp-content/uploads/2016/08/Exploiting-PHP-7-unserialize-Report-160829.pdf](https://blog.checkpoint.com/wp-content/uploads/2016/08/Exploiting-PHP-7-unserialize-Report-160829.pdf)

[https://comsecuris.com/blog/posts/vmware\_vgpu\_shader\_vulnerabilities/](https://comsecuris.com/blog/posts/vmware_vgpu_shader_vulnerabilities/)

[https://github.com/nccgroup/CVE-2017-8759/](https://github.com/nccgroup/CVE-2017-8759/)

[https://blog.bjornweb.nl/2017/08/flash-remote-sandbox-escape-windows-user-credentials-leak/](https://blog.bjornweb.nl/2017/08/flash-remote-sandbox-escape-windows-user-credentials-leak/)

[https://blogs.technet.microsoft.com/enterprisemobility/2017/09/18/active-directory-access-control-list-attacks-and-defense/](https://blogs.technet.microsoft.com/enterprisemobility/2017/09/18/active-directory-access-control-list-attacks-and-defense/)

[https://hatriot.github.io/blog/2017/09/19/abusing-delay-load-dll/](https://hatriot.github.io/blog/2017/09/19/abusing-delay-load-dll/)

[https://github.com/deroko/activationcontexthook](https://github.com/deroko/activationcontexthook)

[http://www.synacktiv.ninja/posts/exploit/rce-vulnerability-in-hp-ilo.html](http://www.synacktiv.ninja/posts/exploit/rce-vulnerability-in-hp-ilo.html)

[https://kvakil.github.io/ropchain.html](https://kvakil.github.io/ropchain.html)

[https://duo.com/assets/ebooks/Duo-Labs-The-Apple-of-Your-EFI.pdf](https://duo.com/assets/ebooks/Duo-Labs-The-Apple-of-Your-EFI.pdf)

[https://googleprojectzero.blogspot.de/2017/09/over-air-vol-2-pt-1-exploiting-wi-fi.html](https://googleprojectzero.blogspot.de/2017/09/over-air-vol-2-pt-1-exploiting-wi-fi.html)

[https://www.zerodayinitiative.com/blog/2017/9/26/duck-assisted-code-execution-in-emc-data-protection-advisor](https://www.zerodayinitiative.com/blog/2017/9/26/duck-assisted-code-execution-in-emc-data-protection-advisor)

[https://specterops.io/assets/resources/SpecterOps\_Subverting\_Trust\_in\_Windows.pdf](https://specterops.io/assets/resources/SpecterOps_Subverting_Trust_in_Windows.pdf)

[https://securingtomorrow.mcafee.com/mcafee-labs/microsoft-kills-potential-remote-code-execution-vulnerability-in-office-cve-2017-8630/\#sf115825366](https://securingtomorrow.mcafee.com/mcafee-labs/microsoft-kills-potential-remote-code-execution-vulnerability-in-office-cve-2017-8630/#sf115825366)

[https://www.zerodayinitiative.com/blog/2017/10/04/vmware-escapology-how-to-houdini-the-hypervisor](https://www.zerodayinitiative.com/blog/2017/10/04/vmware-escapology-how-to-houdini-the-hypervisor)

[https://www.fidusinfosec.com/tp-link-remote-code-execution-cve-2017-13772/](https://www.fidusinfosec.com/tp-link-remote-code-execution-cve-2017-13772/)

[https://www.talosintelligence.com/reports/TALOS-2017-0432](https://www.talosintelligence.com/reports/TALOS-2017-0432)

[https://googleprojectzero.blogspot.de/2017/10/over-air-vol-2-pt-3-exploiting-wi-fi.html](https://googleprojectzero.blogspot.de/2017/10/over-air-vol-2-pt-3-exploiting-wi-fi.html)

[https://tyranidslair.blogspot.de/2017/10/bypassing-sacl-auditing-on-lsass.html](https://tyranidslair.blogspot.de/2017/10/bypassing-sacl-auditing-on-lsass.html)

[https://www.zerodayinitiative.com/blog/2017/10/17/wrapping-the-converter-within-foxit-reader](https://www.zerodayinitiative.com/blog/2017/10/17/wrapping-the-converter-within-foxit-reader)

[https://blogs.technet.microsoft.com/mmpc/2017/10/18/browser-security-beyond-sandboxing/](https://blogs.technet.microsoft.com/mmpc/2017/10/18/browser-security-beyond-sandboxing/)

[https://www.cyberark.com/threat-research-blog/boundhook-exception-based-kernel-controlled-usermode-hooking/](https://www.cyberark.com/threat-research-blog/boundhook-exception-based-kernel-controlled-usermode-hooking/)

[https://hvinternals.blogspot.de/2015/10/hyper-v-debugging-for-beginners.html](https://hvinternals.blogspot.de/2015/10/hyper-v-debugging-for-beginners.html)

[https://hvinternals.blogspot.de/2017/10/hyper-v-debugging-for-beginners-part-2.html](https://hvinternals.blogspot.de/2017/10/hyper-v-debugging-for-beginners-part-2.html)

[https://theevilbit.blogspot.de/2017/10/abusing-gdi-objects-bitmap-objects-size.html](https://theevilbit.blogspot.de/2017/10/abusing-gdi-objects-bitmap-objects-size.html)

[https://securingtomorrow.mcafee.com/mcafee-labs/analyzing-microsoft-office-zero-day-exploit-cve-2017-11826-memory-corruption-vulnerability](https://securingtomorrow.mcafee.com/mcafee-labs/analyzing-microsoft-office-zero-day-exploit-cve-2017-11826-memory-corruption-vulnerability)

[https://www.zerodayinitiative.com/blog/2017/10/27/on-the-trail-to-mobile-pwn2own](https://www.zerodayinitiative.com/blog/2017/10/27/on-the-trail-to-mobile-pwn2own)

[https://nickbloor.co.uk/2017/10/22/analysis-of-cve-2017-12628/](https://nickbloor.co.uk/2017/10/22/analysis-of-cve-2017-12628/)

[https://www.zerodayinitiative.com/blog/2017/8/24/deconstructing-a-winning-webkit-pwn2own-entry](https://www.zerodayinitiative.com/blog/2017/8/24/deconstructing-a-winning-webkit-pwn2own-entry)

[https://riscybusiness.wordpress.com/2017/10/07/hiding-your-process-from-sysinternals/](https://riscybusiness.wordpress.com/2017/10/07/hiding-your-process-from-sysinternals/)

[https://statuscode.ch/2017/11/from-markdown-to-rce-in-atom/](https://statuscode.ch/2017/11/from-markdown-to-rce-in-atom/)

[https://hackernoon.com/afl-unicorn-part-2-fuzzing-the-unfuzzable-bea8de3540a5](https://hackernoon.com/afl-unicorn-part-2-fuzzing-the-unfuzzable-bea8de3540a5)

[https://signal11.io/index.php/2017/11/19/attacking-uninitialized-variables-with-recursion/](https://signal11.io/index.php/2017/11/19/attacking-uninitialized-variables-with-recursion/)

[https://bugs.chromium.org/p/project-zero/issues/detail?id=1332](https://bugs.chromium.org/p/project-zero/issues/detail?id=1332)

[https://enigma0x3.net/2017/01/05/lateral-movement-using-the-mmc20-application-com-object/](https://enigma0x3.net/2017/01/05/lateral-movement-using-the-mmc20-application-com-object/)

[https://posts.specterops.io/lateral-movement-using-outlooks-createobject-method-and-dotnettojscript-a88a81df27eb](https://posts.specterops.io/lateral-movement-using-outlooks-createobject-method-and-dotnettojscript-a88a81df27eb)

[https://bugs.chromium.org/p/chromium/issues/detail?id=766253](https://bugs.chromium.org/p/chromium/issues/detail?id=766253)

[https://embedi.com/blog/skeleton-closet-ms-office-vulnerability-you-didnt-know-about](https://embedi.com/blog/skeleton-closet-ms-office-vulnerability-you-didnt-know-about)

[https://salls.github.io/Linux-Kernel-CVE-2017-5123/](https://salls.github.io/Linux-Kernel-CVE-2017-5123/)

[https://pleasestopnamingvulnerabilities.com/](https://pleasestopnamingvulnerabilities.com/)

[https://fail0verflow.com/blog/2017/ps4-crashdump-dump/](https://fail0verflow.com/blog/2017/ps4-crashdump-dump/)

[https://github.com/Cryptogenic/PS4-4.05-Kernel-Exploit](https://github.com/Cryptogenic/PS4-4.05-Kernel-Exploit)

[https://googleprojectzero.blogspot.de/2017/12/apacolypse-now-exploiting-windows-10-in\_18.html](https://googleprojectzero.blogspot.de/2017/12/apacolypse-now-exploiting-windows-10-in_18.html)

[https://sww-it.ru/2017-11-06/1493](https://sww-it.ru/2017-11-06/1493)

[https://bugs.chromium.org/p/chromium/issues/detail?id=766253](https://bugs.chromium.org/p/chromium/issues/detail?id=766253)

[https://blog.xpnsec.com/windows-warbird-privesc/](https://blog.xpnsec.com/windows-warbird-privesc/)

[https://blog.fortinet.com/2017/11/22/cve-2017-11826-exploited-in-the-wild-with-politically-themed-rtf-document](https://blog.fortinet.com/2017/11/22/cve-2017-11826-exploited-in-the-wild-with-politically-themed-rtf-document)

[https://posts.specterops.io/adventures-in-extremely-strict-device-guard-policy-configuration-part-1-device-drivers-fd1a281b35a8](https://posts.specterops.io/adventures-in-extremely-strict-device-guard-policy-configuration-part-1-device-drivers-fd1a281b35a8)

[https://medium.com/bindecy/huge-dirty-cow-cve-2017-1000405-110eca132de0](https://medium.com/bindecy/huge-dirty-cow-cve-2017-1000405-110eca132de0)

[http://blog.talosintelligence.com/2017/11/exploiting-cve-2016-2334.html](http://blog.talosintelligence.com/2017/11/exploiting-cve-2016-2334.html)

[https://www.crowdstrike.com/blog/badrabbit-ms17-010-exploitation-part-two-elevate-privileges/](https://www.crowdstrike.com/blog/badrabbit-ms17-010-exploitation-part-two-elevate-privileges/)

[https://www.zerodayinitiative.com/blog/2017/12/22/a-matching-pair-of-use-after-free-bugs-in-chakra-asmjs](https://www.zerodayinitiative.com/blog/2017/12/22/a-matching-pair-of-use-after-free-bugs-in-chakra-asmjs)

[https://www.zerodayinitiative.com/blog/2017/12/20/invariantly-exploitable-input-an-apple-safari-bug-worth-revisiting](https://www.zerodayinitiative.com/blog/2017/12/20/invariantly-exploitable-input-an-apple-safari-bug-worth-revisiting)

[https://www.zerodayinitiative.com/blog/2017/12/21/vmwares-launch-escape-system](https://www.zerodayinitiative.com/blog/2017/12/21/vmwares-launch-escape-system)

[https://www.zerodayinitiative.com/blog/2017/12/19/apache-groovy-deserialization-a-cunning-exploit-chain-to-bypass-a-patch](https://www.zerodayinitiative.com/blog/2017/12/19/apache-groovy-deserialization-a-cunning-exploit-chain-to-bypass-a-patch)

[https://blog.blazeinfosec.com/leveraging-web-application-vulnerabilities-to-steal-ntlm-hashes-2/](https://blog.blazeinfosec.com/leveraging-web-application-vulnerabilities-to-steal-ntlm-hashes-2/)

[https://bugs.chromium.org/p/project-zero/issues/detail?id=1358](https://bugs.chromium.org/p/project-zero/issues/detail?id=1358)

[https://www.zerodayinitiative.com/blog/2017/12/18/reading-backwards-controlling-an-integer-underflow-in-adobe-reader](https://www.zerodayinitiative.com/blog/2017/12/18/reading-backwards-controlling-an-integer-underflow-in-adobe-reader)

[https://quequero.org/2017/11/arm-exploitation-iot-episode-3/](https://quequero.org/2017/11/arm-exploitation-iot-episode-3/)

[https://www.coresecurity.com/blog/making-something-out-zeros-alternative-primitive-windows-kernel-exploitation](https://www.coresecurity.com/blog/making-something-out-zeros-alternative-primitive-windows-kernel-exploitation)

[https://www.tarlogic.com/en/blog/exploiting-word-cve-2017-11826/](https://www.tarlogic.com/en/blog/exploiting-word-cve-2017-11826/)

[https://blogs.bromium.com/browser-isolation-with-microsoft-windows-defender-application-guard/](https://blogs.bromium.com/browser-isolation-with-microsoft-windows-defender-application-guard/)

[http://riscy.business/2017/12/lenovos-unsecured-objects/](http://riscy.business/2017/12/lenovos-unsecured-objects/)

[https://randomascii.wordpress.com/2017/12/10/analyzing-a-confusing-crash/](https://randomascii.wordpress.com/2017/12/10/analyzing-a-confusing-crash/)

[https://github.com/Cryptogenic/Exploit-Writeups/blob/master/PS4](https://github.com/Cryptogenic/Exploit-Writeups/blob/master/PS4)

[https://theevilbit.blogspot.in/2017/12/convert-write-where-kernel-exploits.html](https://theevilbit.blogspot.in/2017/12/convert-write-where-kernel-exploits.html)

[https://sensepost.com/blog/2017/linux-heap-exploitation-intro-series-riding-free-on-the-heap-double-free-attacks/](https://sensepost.com/blog/2017/linux-heap-exploitation-intro-series-riding-free-on-the-heap-double-free-attacks/)

[https://sites.google.com/site/bingsunsec/the-battle-for-protected-memory](https://sites.google.com/site/bingsunsec/the-battle-for-protected-memory)

[https://media.ccc.de/v/34c3-8720-ios\_kernel\_exploitation\_archaeology](https://media.ccc.de/v/34c3-8720-ios_kernel_exploitation_archaeology)

[https://cybellum.com/vulnerability-analysis-type-confusion-microsoft-word-2016/](https://cybellum.com/vulnerability-analysis-type-confusion-microsoft-word-2016/)

# 2018 (228)

[https://sensepost.com/blog/2018/linux-heap-exploitation-intro-series-bonus-printf-might-be-leaking/](https://sensepost.com/blog/2018/linux-heap-exploitation-intro-series-bonus-printf-might-be-leaking/)

[https://doar-e.github.io/blog/2017/12/01/debugger-data-model/](https://doar-e.github.io/blog/2017/12/01/debugger-data-model/)

[https://blogs.bromium.com/anatomy-of-meltdown-a-technical-journey/](https://blogs.bromium.com/anatomy-of-meltdown-a-technical-journey/)

[https://www.root-me.org/en/Challenges/App-System/](https://www.root-me.org/en/Challenges/App-System/)

[https://rootkits.xyz/blog/2017/06/kernel-setting-up/](https://rootkits.xyz/blog/2017/06/kernel-setting-up/)

[https://randomascii.wordpress.com/2018/01/07/finding-a-cpu-design-bug-in-the-xbox-360/](https://randomascii.wordpress.com/2018/01/07/finding-a-cpu-design-bug-in-the-xbox-360/)

[https://bugs.chromium.org/p/project-zero/issues/detail?id=1272](https://bugs.chromium.org/p/project-zero/issues/detail?id=1272)

[https://googleprojectzero.blogspot.de/2018/01/reading-privileged-memory-with-side.html](https://googleprojectzero.blogspot.de/2018/01/reading-privileged-memory-with-side.html)

[https://security.googleblog.com/2018/01/more-details-about-mitigations-for-cpu\_4.html](https://security.googleblog.com/2018/01/more-details-about-mitigations-for-cpu_4.html)

[http://blog.cyberus-technology.de/posts/2018-01-03-meltdown.html](http://blog.cyberus-technology.de/posts/2018-01-03-meltdown.html)

[https://meltdownattack.com/](https://meltdownattack.com/)

[https://siguza.github.io/IOHIDeous/](https://siguza.github.io/IOHIDeous/)

[https://bruce30262.github.io/2017/12/15/Learning-browser-exploitation-via-33C3-CTF-feuerfuchs-challenge/](https://bruce30262.github.io/2017/12/15/Learning-browser-exploitation-via-33C3-CTF-feuerfuchs-challenge/)

[https://blog.xpnsec.com/windows-warbird-privesc/](https://blog.xpnsec.com/windows-warbird-privesc/)

[http://pythonsweetness.tumblr.com/post/169166980422/the-mysterious-case-of-the-linux-page-table](http://pythonsweetness.tumblr.com/post/169166980422/the-mysterious-case-of-the-linux-page-table)

[https://sandboxescaper.blogspot.de/2018/01/adobe-reader-escape-or-how-to-steal.html](https://sandboxescaper.blogspot.de/2018/01/adobe-reader-escape-or-how-to-steal.html)

[https://blogs.securiteam.com/index.php/archives/3649](https://blogs.securiteam.com/index.php/archives/3649)

[https://samsclass.info/127/127\_S18.shtml](https://samsclass.info/127/127_S18.shtml)

[https://github.com/Coalfire-Research/iOS-11.1.2-15B202-Jailbreak](https://github.com/Coalfire-Research/iOS-11.1.2-15B202-Jailbreak)

[https://blog.quarkslab.com/reverse-engineering-the-win32k-type-isolation-mitigation.html](https://blog.quarkslab.com/reverse-engineering-the-win32k-type-isolation-mitigation.html)

[https://www.mdsec.co.uk/2018/02/adobe-flash-exploitation-then-and-now-from-cve-2015-5119-to-cve-2018-4878/](https://www.mdsec.co.uk/2018/02/adobe-flash-exploitation-then-and-now-from-cve-2015-5119-to-cve-2018-4878/)

[https://www.coresecurity.com/blog/making-something-out-zeros-alternative-primitive-windows-kernel-exploitation](https://www.coresecurity.com/blog/making-something-out-zeros-alternative-primitive-windows-kernel-exploitation)

[http://blog.frizn.fr/glibc/glibc-heap-to-rip](http://blog.frizn.fr/glibc/glibc-heap-to-rip)

[http://blog.ptsecurity.com/2018/02/new-bypass-and-protection-techniques.html](http://blog.ptsecurity.com/2018/02/new-bypass-and-protection-techniques.html)

[https://blog.zimperium.com/cve-2018-4087-poc-escaping-sandbox-misleading-bluetoothd/](https://blog.zimperium.com/cve-2018-4087-poc-escaping-sandbox-misleading-bluetoothd/)

[https://census-labs.com/news/2018/02/28/windows-10-rs2rs3-gdi-data-only-exploitation-tales-offensivecon-2018/](https://census-labs.com/news/2018/02/28/windows-10-rs2rs3-gdi-data-only-exploitation-tales-offensivecon-2018/)

[https://www.zerodayinitiative.com/blog/2018/3/1/vmware-exploitation-through-uninitialized-buffers](https://www.zerodayinitiative.com/blog/2018/3/1/vmware-exploitation-through-uninitialized-buffers)

[https://github.com/Cryptogenic/Exploit-Writeups/blob/master/WebKit/setAttributeNodeNS UAF Write-up.md](https://github.com/Cryptogenic/Exploit-Writeups/blob/master/WebKit/setAttributeNodeNS UAF Write-up.md)

[https://bazad.github.io/2018/03/a-fun-xnu-infoleak/](https://bazad.github.io/2018/03/a-fun-xnu-infoleak/)

[https://bazad.github.io/2018/03/ida-kernelcache-class-reconstruction/](https://bazad.github.io/2018/03/ida-kernelcache-class-reconstruction/)

[https://devco.re/blog/2018/03/06/exim-off-by-one-RCE-exploiting-CVE-2018-6789-en/](https://devco.re/blog/2018/03/06/exim-off-by-one-RCE-exploiting-CVE-2018-6789-en/)

[https://github.com/0xcl/clang-cfi-bypass-techniques/blob/master/README.md](https://github.com/0xcl/clang-cfi-bypass-techniques/blob/master/README.md)

[https://tradahacking.vn/hitcon-2017-ghost-in-the-heap-writeup-ee6384cd0b7](https://tradahacking.vn/hitcon-2017-ghost-in-the-heap-writeup-ee6384cd0b7)

[https://www.fidusinfosec.com/remote-code-execution-cve-2018-5767/](https://www.fidusinfosec.com/remote-code-execution-cve-2018-5767/)

[https://blogs.technet.microsoft.com/srd/2018/04/04/triaging-a-dll-planting-vulnerability/](https://blogs.technet.microsoft.com/srd/2018/04/04/triaging-a-dll-planting-vulnerability/)

[https://www.fortinet.com/blog/threat-research/a-root-cause-analysis-of-cve-2018-0797---rich-text-format-styles.html](https://www.fortinet.com/blog/threat-research/a-root-cause-analysis-of-cve-2018-0797---rich-text-format-styles.html)

[https://blog.trailofbits.com/2018/04/04/vulnerability-modeling-with-binary-ninja/](https://blog.trailofbits.com/2018/04/04/vulnerability-modeling-with-binary-ninja/)

[https://blog.zimperium.com/cve-2017-13253-buffer-overflow-multiple-android-drm-services/](https://blog.zimperium.com/cve-2017-13253-buffer-overflow-multiple-android-drm-services/)

[https://www.zerodayinitiative.com/blog/2018/4/5/quickly-pwned-quickly-patched-details-of-the-mozilla-pwn2own-exploit](https://www.zerodayinitiative.com/blog/2018/4/5/quickly-pwned-quickly-patched-details-of-the-mozilla-pwn2own-exploit)

[https://blog.grimm-co.com/post/heap-overflow-in-the-necp\_client\_action-syscall/](https://blog.grimm-co.com/post/heap-overflow-in-the-necp_client_action-syscall/)

[http://bazad.github.io/2018/04/ios-advanced-kernel-call-jop/](http://bazad.github.io/2018/04/ios-advanced-kernel-call-jop/)

[https://www.fireeye.com/blog/threat-research/2018/04/solving-ad-hoc-problems-with-hex-rays-api.html](https://www.fireeye.com/blog/threat-research/2018/04/solving-ad-hoc-problems-with-hex-rays-api.html)

[https://doar-e.github.io/blog/2017/12/01/debugger-data-model/](https://doar-e.github.io/blog/2017/12/01/debugger-data-model/)

[https://perception-point.io/2018/04/11/breaking-cfi-cve-2015-5122-coop/](https://labs.mwrinfosecurity.com/assets/BlogFiles/apple-safari-wasm-section-vuln-write-up-2018-04-16.pdf)

[https://labs.mwrinfosecurity.com/assets/BlogFiles/apple-safari-wasm-section-vuln-write-up-2018-04-16.pdf](https://labs.mwrinfosecurity.com/assets/BlogFiles/apple-safari-wasm-section-vuln-write-up-2018-04-16.pdf)

[http://blogs.360.cn/blog/how-to-kill-a-firefox-en/](http://blogs.360.cn/blog/how-to-kill-a-firefox-en/)

[https://googleprojectzero.blogspot.de/2018/04/windows-exploitation-tricks-exploiting.html](https://googleprojectzero.blogspot.de/2018/04/windows-exploitation-tricks-exploiting.html)

[https://cloudblogs.microsoft.com/microsoftsecure/2018/04/19/introducing-windows-defender-system-guard-runtime-attestation/](https://cloudblogs.microsoft.com/microsoftsecure/2018/04/19/introducing-windows-defender-system-guard-runtime-attestation/)

[http://0xeb.net/2018/03/using-z3-with-ida-to-simplify-arithmetic-operations-in-functions/](http://0xeb.net/2018/03/using-z3-with-ida-to-simplify-arithmetic-operations-in-functions/)

[https://xiaodaozhi.com/exploit/132.html](https://xiaodaozhi.com/exploit/132.html)

[https://keenlab.tencent.com/en/2018/04/23/A-bunch-of-Red-Pills-VMware-Escapes/](https://keenlab.tencent.com/en/2018/04/23/A-bunch-of-Red-Pills-VMware-Escapes/)

[https://arxiv.org/pdf/1804.08470.pdf](https://arxiv.org/pdf/1804.08470.pdf)

[https://labs.mwrinfosecurity.com/assets/BlogFiles/huawei-mate9pro-pwn2own-write-up-final-2018-04-26.pdf](https://labs.mwrinfosecurity.com/assets/BlogFiles/huawei-mate9pro-pwn2own-write-up-final-2018-04-26.pdf)

[https://www.zerodayinitiative.com/blog/2018/4/25/when-java-throws-you-a-lemon-make-limenade-sandbox-escape-by-type-confusion](https://www.zerodayinitiative.com/blog/2018/4/25/when-java-throws-you-a-lemon-make-limenade-sandbox-escape-by-type-confusion)

[https://research.checkpoint.com/mmap-vulnerabilities-linux-kernel/](https://research.checkpoint.com/mmap-vulnerabilities-linux-kernel/)

[http://moyix.blogspot.de/2018/03/of-bugs-and-baselines.html](http://moyix.blogspot.de/2018/03/of-bugs-and-baselines.html)

[https://medium.com/verichains/integer-overflow-simple-but-not-easy-9ebbc58bbaa5](https://medium.com/verichains/integer-overflow-simple-but-not-easy-9ebbc58bbaa5)

[http://blog.seekintoo.com/chimay-red.html](http://blog.seekintoo.com/chimay-red.html)

[https://landave.io/2018/05/7-zip-from-uninitialized-memory-to-remote-code-execution/](https://landave.io/2018/05/7-zip-from-uninitialized-memory-to-remote-code-execution/)

[https://www.atredis.com/blog/cylance-privilege-escalation-vulnerability](https://www.atredis.com/blog/cylance-privilege-escalation-vulnerability)

[https://research.checkpoint.com/ntlm-credentials-theft-via-pdf-files/](https://research.checkpoint.com/ntlm-credentials-theft-via-pdf-files/)

[http://jndok.github.io/2016/10/04/pegasus-writeup/](http://jndok.github.io/2016/10/04/pegasus-writeup/)

[https://duo.com/blog/apple-imac-pro-and-secure-storage](https://duo.com/blog/apple-imac-pro-and-secure-storage)

[http://sploit3r.xyz/cve-2017-13284-injection-in-configuration-file/](http://sploit3r.xyz/cve-2017-13284-injection-in-configuration-file/)

[https://www.vusec.net/wp-content/uploads/2018/05/glitch.pdf](https://www.vusec.net/wp-content/uploads/2018/05/glitch.pdf)

[https://www.crowdstrike.com/blog/trying-to-dance-the-samba-an-exercise-in-weaponizing-vulnerabilities/](https://www.crowdstrike.com/blog/trying-to-dance-the-samba-an-exercise-in-weaponizing-vulnerabilities/)

[http://blogs.360.cn/blog/save-and-reborn-gdi-data-only-attack-from-win32k-typeisolation-2/](http://blogs.360.cn/blog/save-and-reborn-gdi-data-only-attack-from-win32k-typeisolation-2/)

[https://lifeasageek.github.io/papers/jeon:hextype.pdf](https://lifeasageek.github.io/papers/jeon:hextype.pdf)

[https://www.triplefault.io/2018/05/spurious-db-exceptions-with-pop-ss.html](https://www.triplefault.io/2018/05/spurious-db-exceptions-with-pop-ss.html)

[http://everdox.net/popss.pdf](http://everdox.net/popss.pdf)

[https://securelist.com/root-cause-analysis-of-cve-2018-8174/85486/](https://securelist.com/root-cause-analysis-of-cve-2018-8174/85486/)

[http://blogs.360.cn/blog/cve-2018-8174-en/](http://blogs.360.cn/blog/cve-2018-8174-en/)

[https://googleprojectzero.blogspot.de/2018/05/bypassing-mitigations-by-attacking-jit.html](https://googleprojectzero.blogspot.de/2018/05/bypassing-mitigations-by-attacking-jit.html)

[https://xerub.github.io/ios/iboot/2018/05/10/de-rebus-antiquis.html](https://xerub.github.io/ios/iboot/2018/05/10/de-rebus-antiquis.html)

[https://www.trustwave.com/Resources/SpiderLabs-Blog/CVE-2018-1000136---Electron-nodeIntegration-Bypass/](https://www.trustwave.com/Resources/SpiderLabs-Blog/CVE-2018-1000136---Electron-nodeIntegration-Bypass/)

[https://blog.can.ac/2018/05/11/arbitrary-code-execution-at-ring-0-using-cve-2018-8897/](https://blog.can.ac/2018/05/11/arbitrary-code-execution-at-ring-0-using-cve-2018-8897/)

[https://xiaodaozhi.com/exploit/156.html](https://xiaodaozhi.com/exploit/156.html)

[http://blog.ret2.io/2018/05/16/practical-eth-decompilation/](http://blog.ret2.io/2018/05/16/practical-eth-decompilation/)

[https://blogs.bromium.com/dissecting-pop-ss-vulnerability/](https://blogs.bromium.com/dissecting-pop-ss-vulnerability/)

[https://blogs.technet.microsoft.com/srd/2018/05/21/analysis-and-mitigation-of-speculative-store-bypass-cve-2018-3639/](https://blogs.technet.microsoft.com/srd/2018/05/21/analysis-and-mitigation-of-speculative-store-bypass-cve-2018-3639/)

[https://igorkorkin.blogspot.com/2018/03/hypervisor-based-active-data-protection.html](https://igorkorkin.blogspot.com/2018/03/hypervisor-based-active-data-protection.html)

[https://srcincite.io/blog/2018/05/21/adobe-me-and-a-double-free.html](https://srcincite.io/blog/2018/05/21/adobe-me-and-a-double-free.html)

[https://www.welivesecurity.com/2018/05/15/tale-two-zero-days/](https://www.welivesecurity.com/2018/05/15/tale-two-zero-days/)

[https://www.zerodayinitiative.com/blog/2018/5/21/mindshare-walking-the-windows-kernel-with-ida-python](https://www.zerodayinitiative.com/blog/2018/5/21/mindshare-walking-the-windows-kernel-with-ida-python)

[https://www.contextis.com/blog/wap-just-happened-my-samsung-galaxy](https://www.contextis.com/blog/wap-just-happened-my-samsung-galaxy)

[https://blog.zimperium.com/cve-2018-4087-poc-escaping-sandbox-misleading-bluetoothd/](https://blog.zimperium.com/cve-2018-4087-poc-escaping-sandbox-misleading-bluetoothd/)

[https://www.zerodayinitiative.com/blog/2018/5/29/malicious-intent-using-adobe-acrobats-ocg-setintent](https://www.zerodayinitiative.com/blog/2018/5/29/malicious-intent-using-adobe-acrobats-ocg-setintent)

[https://icrackthecode.github.io/2018/05/29/CVE-2017-2547/](https://icrackthecode.github.io/2018/05/29/CVE-2017-2547/)

[https://www.contextis.com/blog/frag-grenade-a-remote-code-execution-vulnerability-in-the-steam-client](https://www.contextis.com/blog/frag-grenade-a-remote-code-execution-vulnerability-in-the-steam-client)

[https://dougallj.wordpress.com/2018/06/04/writing-a-hex-rays-plugin-vmx-intrinsics/](https://dougallj.wordpress.com/2018/06/04/writing-a-hex-rays-plugin-vmx-intrinsics/)

[http://martin.uy/blog/projects/reverse-engineering/](http://martin.uy/blog/projects/reverse-engineering/)

[https://staaldraad.github.io/post/2018-06-03-cve-2018-11235-git-rce/](https://staaldraad.github.io/post/2018-06-03-cve-2018-11235-git-rce/)

[http://blog.ret2.io/2018/06/05/pwn2own-2018-exploit-development/](http://blog.ret2.io/2018/06/05/pwn2own-2018-exploit-development/)

[https://landave.io/2018/06/f-secure-anti-virus-remote-code-execution-via-solid-rar-unpacking/](https://landave.io/2018/06/f-secure-anti-virus-remote-code-execution-via-solid-rar-unpacking/)

[https://insinuator.net/2018/06/new-release-of-glibc-heap-analysis-plugins/](https://insinuator.net/2018/06/new-release-of-glibc-heap-analysis-plugins/)

[https://www.zerodayinitiative.com/blog/2018/6/7/down-the-rabbit-hole-a-deep-dive-into-an-attack-on-an-rpc-interface](https://www.zerodayinitiative.com/blog/2018/6/7/down-the-rabbit-hole-a-deep-dive-into-an-attack-on-an-rpc-interface)

[http://blog.ret2.io/2018/06/13/pwn2own-2018-vulnerability-discovery/](http://blog.ret2.io/2018/06/13/pwn2own-2018-vulnerability-discovery/)

[https://securingtomorrow.mcafee.com/mcafee-labs/want-to-break-into-a-locked-windows-10-device-ask-cortana-cve-2018-8140/](https://securingtomorrow.mcafee.com/mcafee-labs/want-to-break-into-a-locked-windows-10-device-ask-cortana-cve-2018-8140/)

[https://codewhitesec.blogspot.com/2018/06/cve-2018-0624.html](https://codewhitesec.blogspot.com/2018/06/cve-2018-0624.html)

[http://blog.tetrane.com/2016/11/reversing-f4b-challenge-part1.html](http://blog.tetrane.com/2016/11/reversing-f4b-challenge-part1.html)

[https://www.fortinet.com/blog/threat-research/microsoft-windows-remote-kernel-crash-vulnerability.html](https://www.fortinet.com/blog/threat-research/microsoft-windows-remote-kernel-crash-vulnerability.html)

[https://www.zerodayinitiative.com/blog/2018/6/19/analyzing-an-integer-overflow-in-bitdefender-av-part-1-the-vulnerability](https://www.zerodayinitiative.com/blog/2018/6/19/analyzing-an-integer-overflow-in-bitdefender-av-part-1-the-vulnerability)

[https://www.zerodayinitiative.com/blog/2018/6/21/analyzing-an-integer-overflow-in-bitdefender-av-part-2-the-exploit](https://www.zerodayinitiative.com/blog/2018/6/21/analyzing-an-integer-overflow-in-bitdefender-av-part-2-the-exploit)

[https://blog.ret2.io/2018/06/19/pwn2own-2018-root-cause-analysis/](https://blog.ret2.io/2018/06/19/pwn2own-2018-root-cause-analysis/)

[https://binary.ninja/2018/06/19/fast-track-to-assembler-writing.html](https://binary.ninja/2018/06/19/fast-track-to-assembler-writing.html)

[https://srcincite.io/blog/2018/06/22/foxes-among-us-foxit-reader-vulnerability-discovery-and-exploitation.html](https://srcincite.io/blog/2018/06/22/foxes-among-us-foxit-reader-vulnerability-discovery-and-exploitation.html)

[https://labs.nettitude.com/blog/cve-2018-6851-to-cve-2018-6857-sophos-privilege-escalation-vulnerabilities/](https://labs.nettitude.com/blog/cve-2018-6851-to-cve-2018-6857-sophos-privilege-escalation-vulnerabilities/)

[https://www.zerodayinitiative.com/blog/2018/6/26/mindshare-variant-hunting-with-ida-python](https://www.zerodayinitiative.com/blog/2018/6/26/mindshare-variant-hunting-with-ida-python)

[https://david942j.blogspot.com/2018/06/write-up-google-ctf-2018-pwn420-sandbox.html](https://david942j.blogspot.com/2018/06/write-up-google-ctf-2018-pwn420-sandbox.html)

[https://www.coresecurity.com/blog/playing-relayed-credentials](https://www.coresecurity.com/blog/playing-relayed-credentials)

[https://labs.portcullis.co.uk/blog/exploiting-inherited-file-handles-in-setuid-programs/](https://labs.portcullis.co.uk/blog/exploiting-inherited-file-handles-in-setuid-programs/)

[https://doar-e.github.io/blog/2018/05/17/breaking-ledgerctfs-aes-white-box-challenge/](https://doar-e.github.io/blog/2018/05/17/breaking-ledgerctfs-aes-white-box-challenge/)

[https://www.begin.re/the-workshop](https://www.begin.re/the-workshop)

[https://googleprojectzero.blogspot.com/2018/06/detecting-kernel-memory-disclosure.html](https://googleprojectzero.blogspot.com/2018/06/detecting-kernel-memory-disclosure.html)

[https://vvdveen.com/publications/dimva2018.pdf](https://vvdveen.com/publications/dimva2018.pdf)

[https://bohops.com/2018/06/28/abusing-com-registry-structure-clsid-localserver32-inprocserver32/](https://bohops.com/2018/06/28/abusing-com-registry-structure-clsid-localserver32-inprocserver32/)

[https://cloudblogs.microsoft.com/microsoftsecure/2018/07/02/taking-apart-a-double-zero-day-sample-discovered-in-joint-hunt-with-eset/](https://cloudblogs.microsoft.com/microsoftsecure/2018/07/02/taking-apart-a-double-zero-day-sample-discovered-in-joint-hunt-with-eset/)

[https://securelist.com/delving-deep-into-vbscript-analysis-of-cve-2018-8174-exploitation/86333/](https://securelist.com/delving-deep-into-vbscript-analysis-of-cve-2018-8174-exploitation/86333/)

[https://alephsecurity.com/2018/06/26/spectre-browser-query-cache/](https://alephsecurity.com/2018/06/26/spectre-browser-query-cache/)

[https://azeria-labs.com/process-continuation-shellcode/](https://azeria-labs.com/process-continuation-shellcode/)

[https://cloudblogs.microsoft.com/microsoftsecure/2018/07/11/hawkeye-keylogger-reborn-v8-an-in-depth-campaign-analysis/](https://cloudblogs.microsoft.com/microsoftsecure/2018/07/11/hawkeye-keylogger-reborn-v8-an-in-depth-campaign-analysis/)

[https://blog.ret2.io/2018/07/11/pwn2own-2018-jsc-exploit/](https://blog.ret2.io/2018/07/11/pwn2own-2018-jsc-exploit/)

[http://blogs.360.cn/blog/google-chrome-pdfium-shading-drawing-integer-overflow-lead-to-rce/](http://blogs.360.cn/blog/google-chrome-pdfium-shading-drawing-integer-overflow-lead-to-rce/)

[https://researchcenter.paloaltonetworks.com/2018/07/unit42-analysis-dhcp-client-script-code-execution-vulnerability-cve-2018-1111/](https://researchcenter.paloaltonetworks.com/2018/07/unit42-analysis-dhcp-client-script-code-execution-vulnerability-cve-2018-1111/)

[https://www.zerodayinitiative.com/blog/2018/7/19/mindshare-an-introduction-to-pykd](https://www.zerodayinitiative.com/blog/2018/7/19/mindshare-an-introduction-to-pykd)

[https://keenlab.tencent.com/en/2018/07/19/Exploiting-iOS-11-0-11-3-1-Multi-path-TCP-A-walk-through/](https://keenlab.tencent.com/en/2018/07/19/Exploiting-iOS-11-0-11-3-1-Multi-path-TCP-A-walk-through/)

[https://tyranidslair.blogspot.com/2018/07/uwp-localhost-network-isolation-and-edge.html](https://tyranidslair.blogspot.com/2018/07/uwp-localhost-network-isolation-and-edge.html)

[http://blogs.360.cn/blog/from-a-patched-itw-0day-to-remote-code-execution-part-i-from-patch-to-new-0day/](http://blogs.360.cn/blog/from-a-patched-itw-0day-to-remote-code-execution-part-i-from-patch-to-new-0day/)

[http://deniable.org/reversing/binary-instrumentation](http://deniable.org/reversing/binary-instrumentation)

[https://blog.quarkslab.com/a-story-about-three-bluetooth-vulnerabilities-in-android.html](https://blog.quarkslab.com/a-story-about-three-bluetooth-vulnerabilities-in-android.html)

[http://blog.ret2.io/2018/07/25/pwn2own-2018-safari-sandbox/](http://blog.ret2.io/2018/07/25/pwn2own-2018-safari-sandbox/)

[https://googleprojectzero.blogspot.com/2018/07/drawing-outside-box-precision-issues-in.html](https://googleprojectzero.blogspot.com/2018/07/drawing-outside-box-precision-issues-in.html)

[http://michaellynn.github.io/2018/07/27/booting-secure/](http://michaellynn.github.io/2018/07/27/booting-secure/)

[https://fail0verflow.com/blog/2018/ps4-aeolia/](https://fail0verflow.com/blog/2018/ps4-aeolia/)

[https://labs.nettitude.com/blog/cve-2017-16245-cve-2017-16246-avecto-defendpoint-multiple-vulnerabilities/](https://labs.nettitude.com/blog/cve-2017-16245-cve-2017-16246-avecto-defendpoint-multiple-vulnerabilities/)

[https://www.zerodayinitiative.com/blog/2018/8/01/throwing-shade-analysis-of-a-foxit-integer-overflow](https://www.zerodayinitiative.com/blog/2018/8/01/throwing-shade-analysis-of-a-foxit-integer-overflow)

[https://www.mdsec.co.uk/2018/08/escaping-the-sandbox-microsoft-office-on-macos/](https://www.mdsec.co.uk/2018/08/escaping-the-sandbox-microsoft-office-on-macos/)

[https://www.mdsec.co.uk/2018/02/adobe-flash-exploitation-then-and-now-from-cve-2015-5119-to-cve-2018-4878/](https://www.mdsec.co.uk/2018/02/adobe-flash-exploitation-then-and-now-from-cve-2015-5119-to-cve-2018-4878/)

[https://blog.talosintelligence.com/2018/08/exploitable-or-not-exploitable-using.html](https://blog.talosintelligence.com/2018/08/exploitable-or-not-exploitable-using.html)

[https://ioactive.com/discovering-and-exploiting-a-vulnerability-in-androids-personal-dictionary/](https://ioactive.com/discovering-and-exploiting-a-vulnerability-in-androids-personal-dictionary/)

[https://insights.sei.cmu.edu/cert/2018/08/when-aslr-is-not-really-aslr---the-case-of-incorrect-assumptions-and-bad-defaults.html](https://insights.sei.cmu.edu/cert/2018/08/when-aslr-is-not-really-aslr---the-case-of-incorrect-assumptions-and-bad-defaults.html)

[https://securingtomorrow.mcafee.com/mcafee-labs/linux-kernel-vulnerability-can-lead-to-privilege-escalation-analyzing-cve-2017-1000112/](https://securingtomorrow.mcafee.com/mcafee-labs/linux-kernel-vulnerability-can-lead-to-privilege-escalation-analyzing-cve-2017-1000112/)

[https://landave.io/2018/05/7-zip-from-uninitialized-memory-to-remote-code-execution/](https://landave.io/2018/05/7-zip-from-uninitialized-memory-to-remote-code-execution/)

[https://landave.io/2018/01/7-zip-multiple-memory-corruptions-via-rar-and-zip/](https://landave.io/2018/01/7-zip-multiple-memory-corruptions-via-rar-and-zip/)

[https://landave.io/2017/07/bitdefender-remote-stack-buffer-overflow-via-7z-ppmd/](https://landave.io/2017/07/bitdefender-remote-stack-buffer-overflow-via-7z-ppmd/)

[https://landave.io/2017/08/bitdefender-heap-buffer-overflow-via-7z-lzma/](https://landave.io/2017/08/bitdefender-heap-buffer-overflow-via-7z-lzma/)

[https://www.twistlock.com/2017/07/13/cve-2017-9951-heap-overflow-memcached-server-1-4-38-twistlock-vulnerability-report/](https://www.twistlock.com/2017/07/13/cve-2017-9951-heap-overflow-memcached-server-1-4-38-twistlock-vulnerability-report/)

[http://blogs.360.cn/blog/eos-asset-multiplication-integer-overflow-vulnerability/](http://blogs.360.cn/blog/eos-asset-multiplication-integer-overflow-vulnerability/)

[https://theevilbit.blogspot.com/2017/11/turning-cve-2017-14961-ikarus-antivirus.html](https://theevilbit.blogspot.com/2017/11/turning-cve-2017-14961-ikarus-antivirus.html)

[http://www.greyhathacker.net/?p=1006](http://www.greyhathacker.net/?p=1006)

[https://googleprojectzero.blogspot.com/2018/08/windows-exploitation-tricks-exploiting.html](https://googleprojectzero.blogspot.com/2018/08/windows-exploitation-tricks-exploiting.html)

[https://speakerdeck.com/marcograss/exploitation-of-a-modern-smartphone-baseband-white-paper](https://speakerdeck.com/marcograss/exploitation-of-a-modern-smartphone-baseband-white-paper)

[https://www.zerodayinitiative.com/blog/2018/8/14/voicemail-vandalism-getting-remote-code-execution-on-microsoft-exchange-server](https://www.zerodayinitiative.com/blog/2018/8/14/voicemail-vandalism-getting-remote-code-execution-on-microsoft-exchange-server)

[https://github.com/google/BrokenType](https://github.com/google/BrokenType)

[https://blogs.technet.microsoft.com/virtualization/2018/08/14/hyper-v-hyperclear/](https://blogs.technet.microsoft.com/virtualization/2018/08/14/hyper-v-hyperclear/)

[https://blog.trendmicro.com/trendlabs-security-intelligence/use-after-free-uaf-vulnerability-cve-2018-8373-in-vbscript-engine-affects-internet-explorer-to-run-shellcode/](https://blog.trendmicro.com/trendlabs-security-intelligence/use-after-free-uaf-vulnerability-cve-2018-8373-in-vbscript-engine-affects-internet-explorer-to-run-shellcode/)

[http://4ldebaran.blogspot.com/2018/07/covering-ian-beers-exploit-techniques.html](http://4ldebaran.blogspot.com/2018/07/covering-ian-beers-exploit-techniques.html)

[https://googleprojectzero.blogspot.com/2018/08/the-problems-and-promise-of-webassembly.html](https://googleprojectzero.blogspot.com/2018/08/the-problems-and-promise-of-webassembly.html)

[https://blogs.technet.microsoft.com/srd/2018/08/16/vulnerability-hunting-with-semmle-ql-part-1/](https://blogs.technet.microsoft.com/srd/2018/08/16/vulnerability-hunting-with-semmle-ql-part-1/)

[https://posts.specterops.io/arbitrary-unsigned-code-execution-vector-in-microsoft-workflow-compiler-exe-3d9294bc5efb](https://posts.specterops.io/arbitrary-unsigned-code-execution-vector-in-microsoft-workflow-compiler-exe-3d9294bc5efb)

[https://www.atredis.com/blog/cve-2018-0952-privilege-escalation-vulnerability-in-windows-standard-collector-service](https://www.atredis.com/blog/cve-2018-0952-privilege-escalation-vulnerability-in-windows-standard-collector-service)

[https://securingtomorrow.mcafee.com/mcafee-labs/insight-into-home-automation-reveals-vulnerability-in-simple-iot-product/](https://securingtomorrow.mcafee.com/mcafee-labs/insight-into-home-automation-reveals-vulnerability-in-simple-iot-product/)

[https://lgtm.com/blog/apache_struts_CVE-2018-11776](https://lgtm.com/blog/apache_struts_CVE-2018-11776)

[https://medium.com/0xcc/cve-2018-8412-ms-office-2016-for-mac-privilege-escalation-via-a-legacy-package-7fccdbf71d9b](https://medium.com/0xcc/cve-2018-8412-ms-office-2016-for-mac-privilege-escalation-via-a-legacy-package-7fccdbf71d9b)

[https://www.contrastsecurity.com/security-influencers/cve-2018-15685](https://www.contrastsecurity.com/security-influencers/cve-2018-15685)

[https://objective-see.com/blog/blog_0x36.html](https://objective-see.com/blog/blog_0x36.html)

[https://www.voidsecurity.in/2018/08/from-compiler-optimization-to-code.html](https://www.voidsecurity.in/2018/08/from-compiler-optimization-to-code.html)

[https://blog.ret2.io/2018/08/28/pwn2own-2018-sandbox-escape/](https://blog.ret2.io/2018/08/28/pwn2own-2018-sandbox-escape/)

[https://raphlinus.github.io/programming/rust/2018/08/17/undefined-behavior.html](https://raphlinus.github.io/programming/rust/2018/08/17/undefined-behavior.html)

[http://mattwarren.org/2018/08/28/Fuzzing-the-.NET-JIT-Compiler/](http://mattwarren.org/2018/08/28/Fuzzing-the-.NET-JIT-Compiler/)

[https://www.zerodayinitiative.com/blog/2018/8/28/virtualbox-3d-acceleration-an-accelerated-attack-surface](https://www.zerodayinitiative.com/blog/2018/8/28/virtualbox-3d-acceleration-an-accelerated-attack-surface)

[https://securify.nl/blog/SFY20180801/click-me-if-you-can_-office-social-engineering-with-embedded-objects.html](https://securify.nl/blog/SFY20180801/click-me-if-you-can_-office-social-engineering-with-embedded-objects.html)

[https://www.nccgroup.trust/uk/our-research/technical-advisory-bypassing-workflows-protection-mechanisms-remote-code-execution-on-sharepoint/](https://www.nccgroup.trust/uk/our-research/technical-advisory-bypassing-workflows-protection-mechanisms-remote-code-execution-on-sharepoint/)

[https://objective-see.com/blog/blog_0x38.html](https://objective-see.com/blog/blog_0x38.html)

[https://blog.0patch.com/2018/08/how-we-micropatched-publicly-dropped.html](https://blog.0patch.com/2018/08/how-we-micropatched-publicly-dropped.html)

[https://srcincite.io/blog/2018/08/31/you-cant-contain-me-analyzing-and-exploiting-an-elevation-of-privilege-in-docker-for-windows.html](https://srcincite.io/blog/2018/08/31/you-cant-contain-me-analyzing-and-exploiting-an-elevation-of-privilege-in-docker-for-windows.html)

[https://zwclose.github.io/fguard-exploit/](https://zwclose.github.io/fguard-exploit/)

[https://wwws.nightwatchcybersecurity.com/blog/](https://wwws.nightwatchcybersecurity.com/blog/)

[https://github.com/niklasb/3dpwn](https://github.com/niklasb/3dpwn)

[https://phoenhex.re/2018-07-27/better-slow-than-sorry](https://phoenhex.re/2018-07-27/better-slow-than-sorry)

[https://blog.ret2.io/2018/09/11/scalable-security-education/](https://blog.ret2.io/2018/09/11/scalable-security-education/)

[https://blog.talosintelligence.com/2018/09/vulnerability-spotlight-Multi-provider-VPN-Client-Privilege-Escalation.html](https://blog.talosintelligence.com/2018/09/vulnerability-spotlight-Multi-provider-VPN-Client-Privilege-Escalation.html)

[http://blogs.360.cn/post/indepth_CVE-2018-5002_en.html](http://blogs.360.cn/post/indepth_CVE-2018-5002_en.html)

[https://blog.exodusintel.com/2018/09/10/truekey-the-not-so-uncommon-story-of-a-failed-patch/](https://blog.exodusintel.com/2018/09/10/truekey-the-not-so-uncommon-story-of-a-failed-patch/)

[https://googleprojectzero.blogspot.com/2018/09/oatmeal-on-universal-cereal-bus.html](https://googleprojectzero.blogspot.com/2018/09/oatmeal-on-universal-cereal-bus.html)

[https://github.com/TheOfficialFloW/h-encore/blob/master/WRITE-UP.md](https://github.com/TheOfficialFloW/h-encore/blob/master/WRITE-UP.md)

[https://www.cybereason.com/blog/.net-malware-dropper](https://www.cybereason.com/blog/.net-malware-dropper)

[https://labs.nettitude.com/blog/cve-2018-5240-symantec-management-agent-altiris-privilege-escalation/](https://labs.nettitude.com/blog/cve-2018-5240-symantec-management-agent-altiris-privilege-escalation/)

[https://medium.com/tenable-techblog/advantech-webaccess-unpatched-rce-ffe9f37f8b83](https://medium.com/tenable-techblog/advantech-webaccess-unpatched-rce-ffe9f37f8b83)

[https://www.qualcomm.com/media/documents/files/whitepaper-pointer-authentication-on-armv8-3.pdf](https://www.qualcomm.com/media/documents/files/whitepaper-pointer-authentication-on-armv8-3.pdf)

[https://blog.exodusintel.com/2018/09/13/to-traverse-or-not-to-that-is-the-question/](https://blog.exodusintel.com/2018/09/13/to-traverse-or-not-to-that-is-the-question/)

[https://www.zerodayinitiative.com/blog/2018/9/13/pivot-pivot-reaching-unreachable-vulnerable-code-in-industrial-iot-platforms](https://www.zerodayinitiative.com/blog/2018/9/13/pivot-pivot-reaching-unreachable-vulnerable-code-in-industrial-iot-platforms)

[https://www.microsoft.com/en-us/msrc/windows-security-servicing-criteria](https://www.microsoft.com/en-us/msrc/windows-security-servicing-criteria)

[https://blog.quarkslab.com/modern-jailbreaks-post-exploitation.html](https://blog.quarkslab.com/modern-jailbreaks-post-exploitation.html)

[https://github.com/externalist/exploit_playground](https://github.com/externalist/exploit_playground)

[https://lgtm.com/blog/rsyslog_snprintf_CVE-2018-1000140](https://lgtm.com/blog/rsyslog_snprintf_CVE-2018-1000140)

[https://lgtm.com/security/disclosures](https://lgtm.com/security/disclosures)

[https://xlab.tencent.com/en/2015/08/27/poking-a-hole-in-the-patch/](https://xlab.tencent.com/en/2015/08/27/poking-a-hole-in-the-patch/)

[https://blogs.technet.microsoft.com/srd/2018/03/23/kva-shadow-mitigating-meltdown-on-windows/](https://blogs.technet.microsoft.com/srd/2018/03/23/kva-shadow-mitigating-meltdown-on-windows/)

[https://github.com/yannayl/glibc_malloc_for_exploiters](https://github.com/yannayl/glibc_malloc_for_exploiters)

[https://github.com/Cryptogenic/Exploit-Writeups/blob/master/FreeBSD/PS4%204.55%20BPF%20Race%20Condition%20Kernel%20Exploit%20Writeup.md](https://github.com/Cryptogenic/Exploit-Writeups/blob/master/FreeBSD/PS4%204.55%20BPF%20Race%20Condition%20Kernel%20Exploit%20Writeup.md)

[https://medium.com/@alex91ar/breaking-the-unbreakable-voting-machine-bluefrost-ekoparty-stack-overflow-challenge-1d6f4a255efe](https://medium.com/@alex91ar/breaking-the-unbreakable-voting-machine-bluefrost-ekoparty-stack-overflow-challenge-1d6f4a255efe)

[https://blogs.technet.microsoft.com/srd/2018/03/15/mitigating-speculative-execution-side-channel-hardware-vulnerabilities/](https://blogs.technet.microsoft.com/srd/2018/03/15/mitigating-speculative-execution-side-channel-hardware-vulnerabilities/)

[https://blogs.technet.microsoft.com/askpfeplat/2017/04/24/windows-10-memory-protection-features/](https://blogs.technet.microsoft.com/askpfeplat/2017/04/24/windows-10-memory-protection-features/)

[https://www.elttam.com.au/blog/goahead/](https://www.elttam.com.au/blog/goahead/)

[https://media.blackhat.com/bh-us-12/Briefings/Tsai/BH_US_12_Tsai_Pan_Exploiting_Windows8_WP.pdf](https://media.blackhat.com/bh-us-12/Briefings/Tsai/BH_US_12_Tsai_Pan_Exploiting_Windows8_WP.pdf)

[https://www.blackhat.com/docs/us-14/materials/us-14-Gorenc-Thinking-Outside-The-Sandbox-Violating-Trust-Boundaries-In-Uncommon-Ways-WP.pdf](https://www.blackhat.com/docs/us-14/materials/us-14-Gorenc-Thinking-Outside-The-Sandbox-Violating-Trust-Boundaries-In-Uncommon-Ways-WP.pdf)

[https://fail0verflow.com/blog/2017/ps4-namedobj-exploit/](https://fail0verflow.com/blog/2017/ps4-namedobj-exploit/)

[https://trusslab.github.io/sugar/webgl_bugs.html](https://trusslab.github.io/sugar/webgl_bugs.html)

[https://gamozolabs.github.io/fuzzing/2018/09/16/scaling_afl.html](https://gamozolabs.github.io/fuzzing/2018/09/16/scaling_afl.html)

[https://www.zerodayinitiative.com/blog/2018/9/18/cve-2018-12794-using-type-confusion-to-get-code-execution-in-adobe-reader](https://www.zerodayinitiative.com/blog/2018/9/18/cve-2018-12794-using-type-confusion-to-get-code-execution-in-adobe-reader)

[https://www.zerodayinitiative.com/blog/2018/9/20/zdi-can-6135-a-remote-code-execution-vulnerability-in-the-microsoft-windows-jet-database-engine](https://www.zerodayinitiative.com/blog/2018/9/20/zdi-can-6135-a-remote-code-execution-vulnerability-in-the-microsoft-windows-jet-database-engine)

[http://www.hexblog.com/?p=1248](http://www.hexblog.com/?p=1248)

[https://trapmine.com/blog/details-of-cve-2018-8410-windows-kernel-vulnerability-discovered-by-trapmine/](https://trapmine.com/blog/details-of-cve-2018-8410-windows-kernel-vulnerability-discovered-by-trapmine/)

[https://medium.com/@deusexmachina667/the-big-blog-post-of-information-security-training-materials-ad9572223fcd](https://medium.com/@deusexmachina667/the-big-blog-post-of-information-security-training-materials-ad9572223fcd)

[https://www.x41-dsec.de/lab/blog/fax/](https://www.x41-dsec.de/lab/blog/fax/)

[http://blogs.360.cn/post/Microsoft%20Edge%20Chakra%20OP_NewScObjArray%20Type%20Confusion%20%E8%BF%9C%E7%A8%8B%E4%BB%A3%E7%A0%81%E6%89%A7%E8%A1%8C%E6%BC%8F%E6%B4%9E%E5%88%86%E6%9E%90%E4%B8%8E%E5%88%A9%E7%94%A8.html](http://blogs.360.cn/post/Microsoft%20Edge%20Chakra%20OP_NewScObjArray%20Type%20Confusion%20%E8%BF%9C%E7%A8%8B%E4%BB%A3%E7%A0%81%E6%89%A7%E8%A1%8C%E6%BC%8F%E6%B4%9E%E5%88%86%E6%9E%90%E4%B8%8E%E5%88%A9%E7%94%A8.html) (CN)

[https://blogs.projectmoon.pw/2018/09/15/Edge-Inline-Segment-Use-After-Free/](https://blogs.projectmoon.pw/2018/09/15/Edge-Inline-Segment-Use-After-Free/) (CN)

[https://googleprojectzero.blogspot.com/2018/09/a-cache-invalidation-bug-in-linux.html](https://googleprojectzero.blogspot.com/2018/09/a-cache-invalidation-bug-in-linux.html)

[https://seclists.org/oss-sec/2018/q3/274](https://seclists.org/oss-sec/2018/q3/274)

[https://cyseclabs.com/blog/linux-kernel-heap-spray](https://cyseclabs.com/blog/linux-kernel-heap-spray)

[https://research.kudelskisecurity.com/2018/09/25/analyzing-arm-cortex-based-mcu-firmwares-using-binary-ninja/](https://research.kudelskisecurity.com/2018/09/25/analyzing-arm-cortex-based-mcu-firmwares-using-binary-ninja/)

[https://lgtm.com/blog/apache_struts_CVE-2018-11776-part2](https://lgtm.com/blog/apache_struts_CVE-2018-11776-part2)

[https://phoenhex.re/2018-09-26/safari-array-concat](https://phoenhex.re/2018-09-26/safari-array-concat)

[https://medium.com/@jimmysong/bitcoin-core-bug-cve-2018-17144-an-analysis-f80d9d373362](https://medium.com/@jimmysong/bitcoin-core-bug-cve-2018-17144-an-analysis-f80d9d373362)

[https://blog.araj.me/state-of-memory-safety-in-linux/](https://blog.araj.me/state-of-memory-safety-in-linux/)

[https://github.com/IOActive/FuzzNDIS/](https://github.com/IOActive/FuzzNDIS/)

[http://phrack.org/papers/escaping_the_java_sandbox.html](http://phrack.org/papers/escaping_the_java_sandbox.html)

[https://secure2.sophos.com/en-us/medialibrary/Gated-Assets/white-papers/Sophos-Comprehensive-Exploit-Prevention-wpna.pdf](https://secure2.sophos.com/en-us/medialibrary/Gated-Assets/white-papers/Sophos-Comprehensive-Exploit-Prevention-wpna.pdf)

[http://0xdabbad00.com/wp-content/uploads/2013/04/exploit_mitigation_kill_chain.pdf](http://0xdabbad00.com/wp-content/uploads/2013/04/exploit_mitigation_kill_chain.pdf)

[https://www.zerodayinitiative.com/blog/2018/9/28/onix-finding-pokmon-in-your-acrobat-revealing-a-new-attack-surface](https://www.zerodayinitiative.com/blog/2018/9/28/onix-finding-pokmon-in-your-acrobat-revealing-a-new-attack-surface)

[https://www.synacktiv.com/ressources/advisories/Vectra_Cognito_cve_2018_14889_14890_14891.pdf](https://www.synacktiv.com/ressources/advisories/Vectra_Cognito_cve_2018_14889_14890_14891.pdf)

[http://blog.ptsecurity.com/2018/10/intel-me-manufacturing-mode-macbook.html](http://blog.ptsecurity.com/2018/10/intel-me-manufacturing-mode-macbook.html)

[https://blog.lexfo.fr/cve-2017-11176-linux-kernel-exploitation-part1.html](https://blog.lexfo.fr/cve-2017-11176-linux-kernel-exploitation-part1.html)

[https://blog.lexfo.fr/cve-2017-11176-linux-kernel-exploitation-part2.html](https://blog.lexfo.fr/cve-2017-11176-linux-kernel-exploitation-part2.html)

[https://blog.lexfo.fr/cve-2017-11176-linux-kernel-exploitation-part3.html](https://blog.lexfo.fr/cve-2017-11176-linux-kernel-exploitation-part3.html)

[https://blog.lexfo.fr/cve-2017-11176-linux-kernel-exploitation-part4.html](https://blog.lexfo.fr/cve-2017-11176-linux-kernel-exploitation-part4.html)

[https://osandamalith.com/2018/02/01/exploiting-format-strings-in-windows/](https://osandamalith.com/2018/02/01/exploiting-format-strings-in-windows/)

[https://googleprojectzero.blogspot.com/2018/10/365-days-later-finding-and-exploiting.html](https://googleprojectzero.blogspot.com/2018/10/365-days-later-finding-and-exploiting.html)

[https://blog.eclypsium.com/2018/09/06/insecure-firmware-updates-in-server-management-systems/](https://blog.eclypsium.com/2018/09/06/insecure-firmware-updates-in-server-management-systems/)

[https://depletionmode.com/zircon-process.html](https://depletionmode.com/zircon-process.html)

[https://geosn0w.github.io/Jailbreaks-Demystified/](https://geosn0w.github.io/Jailbreaks-Demystified/)

[https://blogs.projectmoon.pw/2018/10/07/Use-After-Free-in-mDNSOffloadUserClient-kext/](https://blogs.projectmoon.pw/2018/10/07/Use-After-Free-in-mDNSOffloadUserClient-kext/)

[https://drive.google.com/file/d/1v53GCYPxzoZmB1dCop1yJfZgS1wi64dS/view](https://drive.google.com/file/d/1v53GCYPxzoZmB1dCop1yJfZgS1wi64dS/view)

[https://blogs.securiteam.com/index.php/archives/3766](https://blogs.securiteam.com/index.php/archives/3766)

[https://blogs.securiteam.com/index.php/archives/3765](https://blogs.securiteam.com/index.php/archives/3765)

[https://outflank.nl/blog/2018/10/12/sylk-xlm-code-execution-on-office-2011-for-mac/](https://outflank.nl/blog/2018/10/12/sylk-xlm-code-execution-on-office-2011-for-mac/)

[https://tyranidslair.blogspot.com/2018/10/farewell-to-token-stealing-uac-bypass.html](https://tyranidslair.blogspot.com/2018/10/farewell-to-token-stealing-uac-bypass.html)

[https://leucosite.com/Microsoft-Edge-RCE/](https://leucosite.com/Microsoft-Edge-RCE/)

[https://www.pentestpartners.com/security-blog/time-travel-debugging-finding-windows-gdi-flaws/](https://www.pentestpartners.com/security-blog/time-travel-debugging-finding-windows-gdi-flaws/)

[https://labs.nettitude.com/blog/cve-2018-8955-bitdefender-gravityzone-arbitrary-code-execution/](https://labs.nettitude.com/blog/cve-2018-8955-bitdefender-gravityzone-arbitrary-code-execution/)

[https://labs.mwrinfosecurity.com/assets/BlogFiles/huawei-mate9pro-pwn2own-write-up-final-2018-04-26.pdf](https://labs.mwrinfosecurity.com/assets/BlogFiles/huawei-mate9pro-pwn2own-write-up-final-2018-04-26.pdf)

[https://googleprojectzero.blogspot.com/2018/10/injecting-code-into-windows-protected.html](https://googleprojectzero.blogspot.com/2018/10/injecting-code-into-windows-protected.html)

[https://blog.exodusintel.com/2018/10/16/hpe-imc-a-case-study-on-the-reliability-of-security-fixes/](https://blog.exodusintel.com/2018/10/16/hpe-imc-a-case-study-on-the-reliability-of-security-fixes/)

[https://googleprojectzero.blogspot.com/2018/10/deja-xnu.html](https://googleprojectzero.blogspot.com/2018/10/deja-xnu.html)

[https://bugid.skylined.nl/20181017001.html](https://bugid.skylined.nl/20181017001.html)

[https://www.zerodayinitiative.com/blog/2018/10/18/cve-2018-8460-exposing-a-double-free-in-internet-explorer-for-code-execution](https://www.zerodayinitiative.com/blog/2018/10/18/cve-2018-8460-exposing-a-double-free-in-internet-explorer-for-code-execution)

[https://posts.specterops.io/cve-2018-8414-a-case-study-in-responsible-disclosure-ff74c39615ba](https://posts.specterops.io/cve-2018-8414-a-case-study-in-responsible-disclosure-ff74c39615ba)

[https://googleprojectzero.blogspot.com/2018/10/heap-feng-shader-exploiting-swiftshader.html](https://googleprojectzero.blogspot.com/2018/10/heap-feng-shader-exploiting-swiftshader.html)

[https://www.zerodayinitiative.com/blog/2018/10/24/cve-2018-4338-triggering-an-information-disclosure-on-macos-through-a-broadcom-airport-kext](https://www.zerodayinitiative.com/blog/2018/10/24/cve-2018-4338-triggering-an-information-disclosure-on-macos-through-a-broadcom-airport-kext)

[https://www.securepatterns.com/2018/10/cve-2018-14665-xorg-x-server.html](https://www.securepatterns.com/2018/10/cve-2018-14665-xorg-x-server.html)

[https://blogs.projectmoon.pw/2018/10/26/Chakra-JIT-Loop-LandingPad-ImplicitCall-Bypass/](https://blogs.projectmoon.pw/2018/10/26/Chakra-JIT-Loop-LandingPad-ImplicitCall-Bypass/) (CN)

[https://labs.mwrinfosecurity.com/assets/BlogFiles/apple-safari-pwn2own-vuln-write-up-2018-10-29-final.pdf](https://labs.mwrinfosecurity.com/assets/BlogFiles/apple-safari-pwn2own-vuln-write-up-2018-10-29-final.pdf)

[https://blogs.securiteam.com/index.php/archives/3783](https://blogs.securiteam.com/index.php/archives/3783)

[https://blogs.securiteam.com/index.php/archives/3786](https://blogs.securiteam.com/index.php/archives/3786)

[https://shadowfile.inode.link/blog/2018/10/source-level-debugging-the-xnu-kernel/](https://shadowfile.inode.link/blog/2018/10/source-level-debugging-the-xnu-kernel/)

[https://sandboxescaper.blogspot.com/2018/10/reversing-alpc-where-are-your-windows.html](https://sandboxescaper.blogspot.com/2018/10/reversing-alpc-where-are-your-windows.html)

[https://www.zerodayinitiative.com/blog/2018/10/31/preventative-patching-produces-pwn2own-participants-panic](https://www.zerodayinitiative.com/blog/2018/10/31/preventative-patching-produces-pwn2own-participants-panic)

[http://www.phrack.org/papers/viewer_discretion_advised.html](http://www.phrack.org/papers/viewer_discretion_advised.html)

[https://semmle.com/news/apple-xnu-kernel-icmp-nfs-vulnerabilities](https://semmle.com/news/apple-xnu-kernel-icmp-nfs-vulnerabilities)

[https://blog.talosintelligence.com/2018/11/TALOS-2018-0636.html](https://blog.talosintelligence.com/2018/11/TALOS-2018-0636.html)

[https://insinuator.net/2018/11/h2hc2018/](https://insinuator.net/2018/11/h2hc2018/)

[https://wbenny.github.io/2018/11/04/wow64-internals.html](https://wbenny.github.io/2018/11/04/wow64-internals.html)

[https://blog.zimperium.com/cve-2018-9411-new-critical-vulnerability-multiple-high-privileged-android-services/](https://blog.zimperium.com/cve-2018-9411-new-critical-vulnerability-multiple-high-privileged-android-services/)

[https://github.com/MorteNoir1/virtualbox_e1000_0day](https://github.com/MorteNoir1/virtualbox_e1000_0day)

[https://raw.githubusercontent.com/akayn/Bugs/master/ZDI-CAN-5622/README.md](https://raw.githubusercontent.com/akayn/Bugs/master/ZDI-CAN-5622/README.md)

[https://github.com/saelo/pwn2own2018](https://github.com/saelo/pwn2own2018)

[https://blog.xyz.is/2018/enso.html](https://blog.xyz.is/2018/enso.html)
